import wepy from 'wepy';

export const BookUtils = {
  saveReadingProcess(sourceId, chapter) {
    let localBooks = wepy.getStorageSync('localBooks');
    let book = localBooks.filter(item => {
      return item.sourceId === sourceId
    });

    if (book && book.length > 0) {
      wepy.setStorageSync(sourceId, chapter);
    }
  },
  getReadingProcess(sourceId) {
    let chapter = wepy.getStorageSync(sourceId);
    console.log("book==", chapter);
    if (!chapter) {
      console.log("book==", chapter);
      chapter = 0;
    }
    return chapter;
  }
}
